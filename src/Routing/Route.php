<?php

namespace Routing;

class Route
{
    protected $method;
    
    protected $uri;
    
    protected $action;
    
    public $requestedUri;

    /**
     * Instantiate route.
     *
     * @return void
     */
    public function __construct($method, $uri, $action)
    {
        $this->method = $method;

        $this->uri = $uri;

        $this->action = $this->parseAction($action);
    }
    
    /**
     * [description]
     */
    public function parseAction($action)
    {
        if (is_null($action)) {
            return function() {
                throw new Exception('No action specified.');
            };
        }
        
        if (is_callable($action) || is_string($action)) {
            return ['uses' => $action];
        }
        
        return $action;
    }
    
    /**
     * Return true if route regex and current requested uri match.
     *
     * @param  string $route Stored route to maych with requested uri.
     * @param  string $uri   Current uri request.
     * @return boolean
     */
    public function matches($route, $uri)
    {
        $this->routeRegex = $this->compileRegex($route);
        
        $this->requestedUri = $uri;

        return preg_match($this->routeRegex, $this->requestedUri);
    }

    /**
     * Return stored route as regex to be matched.
     *
     * @param  string $route The route to be returned as regex.
     * @return string
     */
    public function compileRegex($route)
    {
        $regex = preg_replace('/\{(\w+?)\}/', '([^/]++)', $route);

        return '#^'.$regex.'$#s';
    }
    
    /**
     * Match parameter with route name keys.
     *
     * @return array
     */
    public function bindParameters()
    {
        $names = $this->parametersName();
        
        $paths = $this->parameterPaths();
        
        $params = [];
        
        for ($i = 0; $i < count($names); $i++) {
            $params[$names[$i]] = $paths[$i+1];
        }
        
        return $params;
    }
    
    /**
     * Return route parameters.
     *
     * @return array
     */
    public function parametersName()
    {
        preg_match_all('#\{(\w+)\}#', $this->uri, $matches);
        
        return $matches[1];
    }

    /**
     * Return path values matching route keys.
     *
     * @return array
     */
    public function parameterPaths()
    {
        preg_match($this->routeRegex, $this->requestedUri, $matches);
        
        return $matches;
    }
    
    /**
     * Return route action.
     *
     * @return object
     */
    public function getAction()
    {
        $params = $this->bindParameters();
        
        if (count($params) > 0) {
            return call_user_func_array($this->action['uses'], $params);
        }
        
        return call_user_func($this->action['uses']);
    }
}