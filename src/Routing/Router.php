<?php

namespace Routing;

class Router
{
    /**
     * The collection of stored routes store in appropriate method key.
     * @var array
     */
    protected $routes = [];
    
    /**
     * Store current group stack.
     * @var array
     */
    public $groupStack = [];
    
    /**
     * Available verbs.
     * @var array
     */
    protected $verbs = ['GET', 'POST'];
    
    /**
     * Add route to collection of GET routes.
     *
     * @param  string $uri
     * @param  array|closure|string $action
     * @return void
     */
    public function get($uri, $action)
    {
        $this->addRoute('GET', $uri, $action);
    }
    
    /**
     * Add route to collection of POST routes.
     *
     * @param  string $uri
     * @param  array|closure|string $action
     * @return void
     */
    public function post($uri, $action)
    {
        $this->addRoute('POST', $uri, $action);
    }
    
    /**
     * Add route to routes collection.
     *
     * @param string $method
     * @param string $uri
     * @param  array|closure|string $action
     * @return void
     */
    public function addRoute($method, $uri, $action)
    {
        $uri = $this->prefix($uri);
        
        $action = $this->resolveAction($action);
        
        $this->routes[$method][$uri] = (new Route($method, $uri, $action));
    }
    
    /**
     * Group routes and pass attributes to child routes.
     *
     * @param  array $attributes
     * @param  closure $callback
     * @return closure
     */
    public function group(array $attributes, $callback)
    {
        $this->updateGroupStack($attributes);
        
        call_user_func($callback, $this);
        
        array_pop($this->groupStack);
    }
    
    /**
     * Update group stack.
     *
     * @param  array $attributes Current attributes from group
     * @return void
     */
    protected function updateGroupStack($attributes)
    {
        if (! empty($this->groupStack)) {
            $attributes = $this->mergeGroup($attributes, end($this->groupStack));
        }
        
        $this->groupStack[] = $attributes;
    }
    
    /**
     * Merge groups.
     *
     * @param  array $new
     * @param  array $old
     * @return array
     */
    protected function mergeGroup($new, $old)
    {
        $new['namespace'] = $this->formatUsesPrefix($new, $old);
        
        $new['prefix'] = $old['prefix'].$new['prefix'];
        
        return array_merge_recursive([], $new);
    }
    
    /**
     * Merge old and new namespaces to "uses" key on route.
     *
     * @param  string $new
     * @param  string $old
     * @return string
     */
    protected function formatUsesPrefix($new, $old)
    {
        if (isset($new['namespace']) && ! empty($new['namespace'])) {
            return $old['namespace'].'\\'.$new['namespace'];
        }
        
        return $old['namespace'];
    }

    /**
     * [resolveAction description]
     */
    protected function resolveAction($action)
    {
        if (is_callable($action)) {
            return ['uses' => $action];
        }

        return $this->prependGroupUses($action);
    }
    
    /**
     * Add prefix to route uri if available.
     *
     * @param  string $uri
     * @return string
     */
    public function prefix($uri)
    {
        $prefix = ($this->getLastGroupPrefix()) ? '/'.trim($this->getLastGroupPrefix(), '/') : '';
        
        $uri = (! empty($prefix) && $uri === '/') ? '' : '/'.trim($uri, '/');
        
        return $prefix.$uri;
    }
    
    /**
     * Retrieve prefix from the previous group.
     *
     * @return string
     */    
    public function getLastGroupPrefix()
    {
        if (! empty($this->groupStack)) {
            $last = end($this->groupStack);

            return isset($last['prefix']) ? $last['prefix'] : '';
        }
        
        return '';
    }
    
    /**
     * Prepend group namespace onto current use if action is not a closure.
     *
     * @param  array|string|closure $action
     * @return string
     */
    protected function prependGroupUses($action)
    {  
        $groupNamespace = $this->getLastGroupNamespace().'\\';
        
        if (is_string($action)) {
            $action = ['uses' => $action];
        }
        
        return trim($groupNamespace.$action['uses'], '\\');
    }
    
    /**
     * Retrieve namespace from the previous group.
     *
     * @return string
     */
    public function getLastGroupNamespace()
    {
        if (! empty($this->groupStack)) {
            $last = end($this->groupStack);

            return isset($last['namespace']) ? $last['namespace'] : '';
        }
        
        return '';
    }
    
    /**
     * Return routes collection.
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }
    
    /**
     * Return current requested URI.
     *
     * @return string
     */
    public function getUri()
    {
        return $_SERVER['REQUEST_URI'];
    }
    
    /**
     * Return current request if it's an available verb.
     *
     * @return string
     */
    public function getMethod()
    {
        $method = $_SERVER['REQUEST_METHOD'];
    
        return (in_array($method, $this->verbs)) ? $method : 'GET';
    }
    
    /**
     * Return requested route and action.
     *
     * @return mixed
     */
    public function dispatch()
    {
        $requestedMethod = $this->getMethod();
        
        $requestedUri = $this->getUri();
        
        if (! array_key_exists($requestedMethod, $this->routes)) {
            throw new \Exception('Requested http method is invalid.');
        }

        $routes = $this->routes[$requestedMethod];
        
        foreach ($routes as $route => $routeInstance) {
            if ($routeInstance->matches($route, $requestedUri)) {
                return $routeInstance->getAction();
            }
        }

        throw new \Exception('Requested route is invalid.');
    }
}