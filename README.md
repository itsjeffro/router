# Simplified php router #

This is just my attempt at creating a simplified php router.

### Instantiate router ###

```
$route = new Routing\Router;

$route->get('/', function() {
    echo 'Home';
});

$route->post('users', function() {
	   echo 'Process user';
});

$route->dispatch();

```

### Group routes ###

```
$route->group(['prefix' => 'users'], function($route) {
    // Routes will have users prependes to uris
});
```

```
$route->group(['namespace' => 'Namespace\Path'], function($route) {
    // Other routes will have Namespace\Path prepended
});
```